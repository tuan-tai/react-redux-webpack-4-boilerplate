import * as PostActions from "../actions/index";

const initialState = {
  posts: [],
  loading: false,
  error: null
};

export default function postReducer(state = initialState, action) {
  switch(action.type) {
    case "POST_REQUEST":
      return {
        ...state,
        loading: true,
        error: null,
      };
    case "POST_SUCCESS":
      return {
        ...state,
        loading: false,
        posts: action.payload
      };
    case "POSTS_FAILURE":
      return {
        ...state,
        loading: false,
        error: action,
        posts: []
      };
    default:
      // ALWAYS have a default case in a reducer
      return state;
  }
}