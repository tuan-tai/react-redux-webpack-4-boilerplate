import React from "react";
import { connect } from "react-redux";
import { put } from "redux-saga/effects";
import { POSTS_REQUEST } from "../actions/index";

class Posts extends React.Component {
  componentDidMount() {
    this.props.dispatch(POSTS_REQUEST());
  }

  render() {
    const { error, loading, photos } = this.props;

    if (error) {
      return <div>Error! {error.message}</div>;
    }

    if (loading) {
      return <div>Loading...</div>;
    }

    return (
      <ul>

      </ul>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts,
  loading: state.photos,
  error: state.photos
});

export default connect(mapStateToProps)(Posts);