export const POSTS_REQUEST = () => {
  return {type: "POSTS_REQUEST"}
};
export const POSTS_SUCCESS = (data) => ({
  type: "POST_SUCCESS",
  payload: data
});
export const POSTS_FAILURE = () => {
  return {type: "POSTS_FAILURE"}
};